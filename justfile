# C Lib
c-build:
    gcc -std=c99 -fPIC -shared -I ./libs src/myclib.c -o libs/myclib.so
c-run:
    cd libs && lua ../src/testc.lua
c-lean:
    rm -rf libs/*
c-info:
   readelf --dyn-syms  libs/myclib.so

# C++ Lib
cpp-build:
    g++ -shared -I ./libs src/mycpplib.cpp -o libs/mycpplib.so
cpp-info:
    readelf --dyn-syms  libs/mycpplib.so
cpp-run:
    cd libs && lua ../src/testcpp.lua

# Rust Lib
rs-build:
    just myrslib/build
    cp myrslib/target/release/libmyrslib.so libs/my_rs_module.so
rs-run:
    cd libs && lua ../src/testrs.lua
rs-info:
    readelf --dyn-syms libs/my_rs_module.so | grep luaopen_


go-build:
    cd mygolib && go build -o ../libs/golib.so -buildmode=c-shared main.go  
go-run:
    cd libs && lua ../src/testgo.lua
go-info:
    readelf --dyn-syms libs/golib.so

go2-build:
    cd mygolib2 && go build -o ../libs/golib2.so -buildmode=c-shared main.go  
go2-run:
    cd libs && lua ../src/testgo2.lua
go2-info:
    readelf --dyn-syms libs/golib2.so