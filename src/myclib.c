
// mylib.c
#include <stdio.h>
#include "lua5.4/lua.h"
#include "lua5.4/lauxlib.h"

static int l_test (lua_State *L) {
    printf("hello world\n");
    return 0;
}

static const struct luaL_Reg alib [] = {
    {"test", l_test},
    {NULL, NULL}
};

// loader函数 myclib 决定 require "myclib" 的命名
int luaopen_myclib(lua_State *L) {
    // 新建一个库(table)，把函数加入这个库中，并返回
    luaL_newlib(L, alib);
    return 1;
}