-- Load the shared library
print(package.loadlib("./golib.so", "Hello"))

local hi = package.loadlib("./golib.so", "Hello")
local sayHello = package.loadlib("./golib.so", "SayHello")


if hi then
    local result = hi()
    print("Result:", result)  
else
    print("Failed to load the shared library")
end

sayHello()