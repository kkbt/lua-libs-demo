
#include <stdio.h>
#include "lua5.4/lua.h"
#include "lua5.4/lauxlib.h"


// 函数必须以C的形式被导出
extern "C"  int hello (lua_State *L) {
    printf("hello world , there is cpp code !");
    return 0;
}

static const struct luaL_Reg alib [] = {
    {"hello", hello},
    {NULL, NULL}
};

// loader函数 myclib 决定 require "myclib" 的命名
// 必须为 luaopen_xxx
extern "C" int luaopen_mycpplib(lua_State *L) {
    // 新建一个库(table)，把函数加入这个库中，并返回
    luaL_newlib(L, alib);
    return 1;
}