package main

// 一些注释是必须的 是给 cgo 使用的参数
// 使用的是低级功能 导出为 c 可用的函数

// #include <stdlib.h>
import "C"
import (
	"fmt"
	"unsafe"
)

func main() { // 占位
}

//export Hello
func Hello() {
	fmt.Println("Hello")
}

//export SayHello
func SayHello(s *C.char) {
	fmt.Println("GoCode: Run SayHello", C.GoString(s))
}

//export Free
func Free(p unsafe.Pointer) {
	C.free(p)
}
