package main

// 一些注释是必须的 是给 cgo 使用的参数
// 使用的是低级功能 导出为 c 可用的函数

//#cgo CFLAGS: -I/usr/include
//#cgo LDFLAGS: -L/lib/x86_64-linux-gnu -llua5.4
//#include "lua5.4/lauxlib.h"
//#include "lua5.4/lua.h"
//#include <stdlib.h>
import "C"
import (
	"fmt"
	"reflect"
	"unsafe"
)

func main() { // 占位
}

//export Hello2
func Hello2(L *C.struct_lua_State) C.int {
	fmt.Println("hello world")
	return 0
}

// ---------- c 代码改写 ----------

//export luaopen_golib2
func luaopen_golib2(L *C.struct_lua_State) C.int {
	// 获取 Hello2 函数的 reflect.Value
	hello2Value := reflect.ValueOf(Hello2)

	// 获取 Hello2 函数的指针
	ptr := unsafe.Pointer(hello2Value.Pointer())

	var myclib_funcs = []C.struct_luaL_Reg{
		{C.CString("Hello2"), (*[0]byte)(ptr)},
		{nil, nil},
	}
	// C.luaL_newlib(L, myclib_funcs)
	// unsafe.Sizeof(myclib_funcs) = 24
	fmt.Println(unsafe.Sizeof(myclib_funcs), unsafe.Sizeof(myclib_funcs[0])) // 24 16 ?
	// C.lua_createtable(L, C.int(0), C.int(unsafe.Sizeof(myclib_funcs)/unsafe.Sizeof(myclib_funcs[0])-1))
	C.lua_createtable(L, C.int(0), C.int(0))

	return 1
}
